package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/mail"

	"biznaideas.com/utils"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

type Email struct {
	FullName     string `json:"full_name"`
	EmailAddress string `json:"email_address"`
	Message      string `json:"message"`
}

func homeLink(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Welcome home!")
}

func sendEmail(w http.ResponseWriter, r *http.Request) {
	var email Email
	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprintf(w, "Kindly fill in all the details")
	}

	json.Unmarshal(reqBody, &email)
	log.Println("Sending email >>> ", email.FullName, email.EmailAddress, email.Message)

	addr := "127.0.0.1:25"
	from := (&mail.Address{"Bizna Ideas", "info@biznaideas.com"}).String()

	// Send message to biznaideas email
	subject := "Thank you for your email."
	body := "Hi BiznaIdeas, New Email received: \n\n<br/><br/>" + email.Message
	// []string{(&mail.Address{"to name", "to@example.com"}).String()}
	to := []string{(&mail.Address{"Bizna Ideas : " + email.FullName, "gidemn@gmail.com"}).String()}
	err = utils.SendMail(addr, from, subject, body, to)

	// Send confirmation email to client
	subject = "Thank you for your email."
	body = "Hi, thank you for your message. Our team will get back to you. \n\n<br/><br/>" + email.Message
	// []string{(&mail.Address{"to name", "to@example.com"}).String()}
	to = []string{(&mail.Address{email.FullName, email.EmailAddress}).String()}

	err = utils.SendMail(addr, from, subject, body, to)

	if err != nil {
		log.Println("Error sending email.", err.Error())
	} else {
		log.Println("Email sent!")
	}

	// json.NewEncoder(w).Encode(email)
	utils.RespondJSON(w, http.StatusCreated, email)
}

func main() {

	router := mux.NewRouter().StrictSlash(true)

	headers := handlers.AllowedHeaders([]string{
		"X-Requested-With", "Content-Type", "Authorization",
		"Access-Control-Allow-Origin", "http://localhost:3000",
		"Access-Control-Allow-Origin", "*"})
	methods := handlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"})
	origins := handlers.AllowedHeaders([]string{"*"})

	router.HandleFunc("/", homeLink)
	router.HandleFunc("/email", sendEmail).Methods("POST")

	host := ":8093"
	log.Println("Starting application on port :", host)
	// log.Fatal(http.ListenAndServe(":8093", router))

	log.Println("Application started on port >>> ", host)
	log.Fatal(http.ListenAndServe(host, handlers.CORS(headers, methods, origins)(router)))
}
